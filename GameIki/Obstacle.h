#pragma once
#include "Object.h"

class Obstacle
{
public:
	Obstacle();
	~Obstacle();

	Position position;
	char obs = 'O';

	void Draw();
	void Clear();
	void randPos();
};

