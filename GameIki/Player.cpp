#include <iostream>
#include "Object.h"
#include "Player.h"
#include "Obstacle.h"
// #include "Windows.h"

using namespace std;

Player::Player()
{
	Object object;
	object.position.x = 15;
	object.position.y = 10;
	object.skin = '@';

	snake.push_back(object);

	object.position.x = 16;
	snake.push_back(object);
	object.position.x = 17;
	snake.push_back(object);
	object.position.x = 18;
	snake.push_back(object);
}

int Player::x()
{
	Position headpos = snake.front().position;
	return headpos.x;
}

int Player::y()
{
	Position headpos = snake.front().position;
	return headpos.y;
}

void Player::draw()
{
	for (Object object : snake)
	{
		Position pos = object.position;
		pos.gotoxy(pos.x, pos.y);
		//gotoxy(pos.x, pos.y);
		cout << object.skin;
		pos.gotoxy(0, 21);
	}

	Position scorePos; 
	scorePos.gotoxy(0, 21);
	cout << "Your Score : " << score << endl;
}

void Player::clear()
{
	for (Object object : snake)
	{
		Position pos = object.position;
		pos.gotoxy(pos.x, pos.y);
		cout << " ";
		pos.gotoxy(0, 21);
	}
}

void Player::keepMoving(char i)
{
	switch(i) {
		case 'a':
			moveLeft();
			break;

		case 'd':
			moveRight();
			break;

		case 'w':
			moveUp();
			break;

		case 's':
			moveDown();
			break;
		default:
			break;
	}
}

int Player::bodyPosX()
{
	Position pos;
	for (Object object : snake)
	{
		pos = object.position;
		//pos.gotoxy(pos.x, pos.y);
		//gotoxy(pos.x, pos.y);
		//cout << object.skin;
		//pos.gotoxy(0, 21);
	}
	
	return pos.x;
}

int Player::bodyPosY()
{
	Position pos;
	for (Object object : snake)
	{
		pos = object.position;
		//pos.gotoxy(pos.x, pos.y);
		//gotoxy(pos.x, pos.y);
		//cout << object.skin;
		//pos.gotoxy(0, 21);
	}

	return pos.y;
}

void Player::addTail()
{
	Obstacle obs;
	Position newTail = obs.position;

	Position tailPos = snake.back().position;

	Object tail;
	tail.position = tailPos;
	snake.push_back(tail);
	
	score++;

}

void Player::moveUp()
{
	Position headPos = snake.front().position;
	headPos.y--;

	Object last = snake.back();
	last.position = headPos;
	snake.pop_back();
	snake.push_front(last);
}

void Player::moveDown()
{
	Position headPos = snake.front().position;
	headPos.y++;

	Object last = snake.back();
	last.position = headPos;
	snake.pop_back();
	snake.push_front(last);
}

void Player::moveLeft()
{
	Position headPos = snake.front().position;
	headPos.x--;

	Object last = snake.back();
	last.position = headPos;
	snake.pop_back();
	snake.push_front(last);
}

void Player::moveRight()
{
	Position headPos = snake.front().position;
	headPos.x++;

	Object last = snake.back();
	last.position = headPos;
	snake.pop_back();
	snake.push_front(last);
}