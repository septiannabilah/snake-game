#pragma once
#include "Board.h"
#include "Player.h"
#include "Obstacle.h"

class GameRules {
private:
	Board arena;
	Player player;
	Obstacle obstacle;

public:
	GameRules();
	void move();
	void colFruit();
	void colWall();
	void colBody();
	void gameOver();

};